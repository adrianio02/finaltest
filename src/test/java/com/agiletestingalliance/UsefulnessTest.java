package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.Usefulness;

public class UsefulnessTest {
    @Test
    public void testDesc() throws Exception {

        String key = new Usefulness().desc();
        assertTrue("STarts with",key.contains("DevOps is about transformation, about building quality in, improving productivity"));
    }
}
