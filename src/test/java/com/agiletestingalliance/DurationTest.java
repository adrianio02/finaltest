package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.Duration;

public class DurationTest {
    @Test
    public void testDesc() throws Exception {

        String key = new Duration().dur();
        assertTrue("STarts with",key.contains("CP-DOF is designed specifically for corporates and working professionals alike"));
    }
}
