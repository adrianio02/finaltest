package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.AboutCPDOF;

public class AboutCPDOFTest {
    @Test
    public void testDesc() throws Exception {

        String key = new AboutCPDOF().desc();
        assertTrue("STarts with",key.contains("CP-DOF certification program covers end to end DevOps Life Cycle practically"));
    }
}
