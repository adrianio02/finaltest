package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.MinMax;

public class MinMaxTest {
    @Test
    public void findMax() throws Exception {

        int key = new MinMax().findMax(6,8);
        assertEquals("BIgger", 8, key);
    }
    
    @Test
    public void findMin() throws Exception {

        int key = new MinMax().findMax(10,8);
        assertEquals("BIgger", 10, key);
    }
}
