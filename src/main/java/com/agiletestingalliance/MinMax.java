package com.agiletestingalliance;

public class MinMax {

    public int findMax(int operand1, int operand2) {
        if (operand2 > operand1)
        {
             return operand2;       	
        }
        else
        {
            return operand1;
        }
    }

}
